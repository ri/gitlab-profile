Welcome to the EBRAINS Research Infrastructure (EBRAINS RI) GitLab group! This space serves as a collaborative platform for researchers, developers, and stakeholders involved in the EBRAINS initiatives.

## About EBRAINS RI

EBRAINS RI is a cutting-edge research infrastructure developed as part of the European Commission's Human Brain Project (HBP). It provides researchers across various disciplines with access to advanced tools, resources, and services for understanding the brain and developing innovative solutions in neuroscience and related fields.

## Purpose of this Group

This GitLab group serves as the central hub for technical and research activities, platform development, and infrastructure management within the EBRAINS project. Here, you'll find resources, guidelines, and collaborative spaces to facilitate your contributions to the EBRAINS RI ecosystem.

## Key Features

[Tech-Hub](https://gitlab.ebrains.eu/ri/tech-hub): A dedicated space for coordinating platform development, sharing technical documentation, and fostering collaboration among developers.  
[Projects & Initiatives](https://gitlab.ebrains.eu/ri/projects-and-initiatives): Explore and contribute to ongoing projects related to EBRAINS.  
[Tech-Issues](https://gitlab.ebrains.eu/groups/ri/tech-hub/-/issues): Report bugs, suggest enhancements, or discuss technical challenges by opening issues in relevant projects within the group.  

**Access Control**: Only owners and maintainers can create new groups within this space. This ensures centralized management and coordination of technical activities. See [How to create a group](https://gitlab.ebrains.eu/ri/tech-hub/content-sites/technical-handbook/content/-/blob/main/docs/version-control/the-ebrains-ri-group/how-to-contribute/how-to-create-a-group/_index.md) and [How to maintain a group](https://gitlab.ebrains.eu/ri/tech-hub/content-sites/technical-handbook/content/-/blob/main/docs/version-control/the-ebrains-ri-group/how-to-contribute/how-to-create-a-group/group-requirements.md#group-maintenance). 

**Project Creation**: Members who are owners of specific sub-groups can create projects within their respective sub-groups. If you're an owner of a sub-group and have a specific project in mind, you can initiate its creation directly within your sub-group's page in the Tech-Hub. However, if you require a dedicated project outside of your group's score or need assistance in setting up a project, please see [How to create a project](https://gitlab.ebrains.eu/ri/tech-hub/content-sites/technical-handbook/content/-/blob/main/docs/version-control/the-ebrains-ri-group/how-to-contribute/how-to-create-project/_index.md).   

## Key Tech-Hub Resources

- [EBRAINS RI Board](https://gitlab.ebrains.eu/groups/ri/-/boards) - Access the central board
- [Roadmaps](https://gitlab.ebrains.eu/ri/tech-hub/coordination/roadmaps/-/wikis/home) - Publish your solution roadmap
- [Milestones](https://gitlab.ebrains.eu/groups/ri/-/milestones) - Monitor the milestones progress

## Contact Us

If you have any questions, feedback, or inquiries regarding EBRAINS RI or this GitLab group, please don't hesitate to reach out to us:

Group Maintainers: support@ebrains.eu
